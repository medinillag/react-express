// importamos la libreria de express
const  express = require('express');
// instalacion la libreria express
const app = express();
//agregamos body-parser
const bodyParser = require('body-parser');
// agregamos el api donde van a estar las rutas del web server
const api=require('./api.js');
//definimos el numero de puerto
const puerto=8080 // por el momento en el 8080
//especificamos el subdirectorio donde se encuentran las páginas estáticas
app.use(express.static(__dirname+'/public'));
//extended: false significa que parsea solo string (no archivos de imagenes por ejemplo)
app.use(bodyParser.urlencoded({ extended: false }));
// agregamos el parceso de json ()
app.use(bodyParser.json());
// agregamos el  Access-Control-Origin es muy imporntate para la comunicacion ente el frondend y no tener problemas
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
//agregamos api a express (las rutas del web server)
app.use(api);
// iniciamos con el servidor
app.listen(puerto,function(){
    console.log("el servidor web esta arriba en el puerto "+puerto);
});