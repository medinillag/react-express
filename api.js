/*
    importamos las librerias 
        # express y Router

*/
const express = require('express');
const router = express.Router();

 
//creamos la direccion inicial del web server
router.get("/",async(req,res)=>{
    res.send("el web server esta arriba ");
})

/*
    este el metodo en donde la aplicacion del frond va a mandar
    los datos recolectados
*/
 router.post("/setDate",async(req,res)=>{
    console.log("si le pego al uri de comunicacion");
    /*
        en la variable
        datos_preguntas obtengo el json de la informacion que 
        vandaron
        ----------------------
        estructura por el momento
        datos=
        {
            pregunta1 :"valor de la pregunta si o no" ,
            pregunta2 :"valor de la pregunta si o no" ,
            geolocalizacion : "valores de la geolocalizacion"
            (falta definir si mandamos latitud y coordenadas o la dirreccion)
        }
        #nota es una propuesta que puede variar  dependiento las preguntas
    */    
    var datos_Preguntas=req.body.datos;
    // imprimo como vine la informacion
    console.log(datos_Preguntas)
    // por el momento esta definida esta respuesta y su estructura
    /*
         estado = ok  #indica que fue insertado exitosamente a la base datos
         mensaje = se resivio y fue procesada
         ------------------------------------------------------------------
         estado = error #indica que ocurio un error en la base datos y no se guardo correctamente
         mensaje = error al procesar
         # nota esto resive la aplicacion frond es neserio que la respuesta sea asi
    */
    var respuesta=
    {
        estado:"ok",
        mensaje:"la informacion se resivio corectamente"
    }  
    // envio la respueta
    res.send(respuesta)
    /*
        aqui en este metodo es donde suguiero que ingresen la informacion
        a la base datos para que tengan problemas al  procesar
        en este metodo
    */
  
    });

  module.exports = router;